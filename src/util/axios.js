import axios from 'axios';
import qs from 'qs';
import env from '@/util/env.js';

// 添加一个请求拦截器 网易云音乐 NodeJS 版API
axios.interceptors.request.use(config => {
  return config;
}, error => {
  return Promise.reject(error);
});

// 添加一个响应拦截器  网易云音乐 NodeJS 版API
axios.interceptors.response.use(response => {
  const res = response.data;
  if (res.code === 200) {
    return res.list;
  } else if (res.code === 406) {
    // tokenId失效
  } else {
    return Promise.reject(res.code);
  }
}, error => {
  if (error && error.response) {
    switch (error.response.status) {
      case 400:
        error.message = '请求错误(400)';
        break;
      case 401:
        error.message = '未授权，请重新登录(401)';
        break;
      default:
        error.message = `连接出错(${error.response.status})!`;
    }
  } else {
    error.message = '连接服务器失败!';
  }
  return Promise.reject(error);
});

export default {
  // post请求
  post(url, data) {
    console.log(url, env.baseURL);
    return axios({
      method: 'post',
      baseURL: env.baseURL,
      url: url,
      data: qs.stringify(data),
      timeout: 1000,
      withCredentials: false, // 是否携带缓存
      headers: {
        'X-Requested-With': 'XMLHttpRequest',
        'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
      }
    }).catch(err => {
      console.log(err);
    });
  },

  // get请求
  get(url, data) {
    return axios({
      method: 'get',
      baseURL: env.baseURL,
      url: url,
      params: data,
      timeout: 1000,
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
    }).catch(err => {
      console.log(err);
    });
  }
};
