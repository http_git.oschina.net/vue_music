/**
 * 配置编译环境和线上环境之间的切换
 * baseUrl:域名地址
 * baseImgPath:图片存放地址
 */

let baseURL = '';
let baseImgPath = '';
if (process.env.NODE_ENV === 'development') {
  baseURL = 'http://localhost:3000';
  baseImgPath = '';
} else {
  baseURL = '';
  baseImgPath = '';
}

module.exports = {
  baseURL,
  baseImgPath
};
