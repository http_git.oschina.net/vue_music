import localJsonp from 'jsonp';

/**
 * 封装jsonp公共方法
 */
export default function jsonp(url, data, options) {
  console.log('-----测试map()方法urlParse2()循环------');
  console.log(urlParse2(data));
  console.log('-----测试findIndex()方法------');
  findIndex();
  console.log('----测试使用for in 循环------');
  url += (url.indexOf('?') < 0 ? '?' : '&') + urlParse(data); // 判断url是否带有?参数 url.indexOf('?') == -1 表示不含有
  return new Promise((resolve, reject) => {
    localJsonp(url, options, (err, data) => {
      if (!err) {
        resolve(data);
      } else {
        reject(err);
      }
    });
  });
};

/**
 * 解析url参数
 */
function urlParse(data) {
  let url = '';
  for (let k in data) {
    let value = data[k] !== 'undefined' ? data[k] : '';
    url += `&${k}=${decodeURIComponent(value)}`;
  }
  return url ? url.substring(1) : url;
};

/**
 * 解析url参数 将对象转化成请求串
 */
export const urlParse2 = (data) => {
  let url = '';
  url = Object.keys(data).map(k => decodeURIComponent(k) + '=' + decodeURIComponent(data[k])).join('&');
  return url;
};

/**
 * 获取数组中某一对象的下标
 */
function findIndex() {
  let posts = [
    {id: '13', title: 'Title 221'},
    {id: '5', title: 'Title 102'},
    {id: '131', title: 'Title 18'},
    {id: '55', title: 'Title 234'}
  ];
  let requireIndex = posts.findIndex(obj => obj.id === '131');
  console.log('使用findIndex()方法进行快速检索的值： ' + requireIndex);
};
