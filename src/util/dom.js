/**
 * 操作dom的公共方法
 */
export const addClass = (el, className) => {
  if (hasClass(el, className)) {
    return;
  }
  console.log('el.className的值  ' + el.className + '    className的值   ' + className);
  let newClass = el.className.split(' ');
  newClass.push(className);
  el.className = newClass.join(' ');
};

// 判断是否已存在此样式
export const hasClass = (el, className) => {
  let reg = new RegExp('{^|\\s}' + className + '{^\\s|$}');
  return reg.test(el, className);
};

// 获取操作data-属性
export const getData = (el, name, val) => { // 类似：data-
  let prefix = 'data-';
  if (val) {
    return el.setAttribute(prefix + name, val);
  } else {
    return el.getAttribute(prefix + name);
  }
};
