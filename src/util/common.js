// jsonp的公共参数
export const commonParam = {
  g_tk: 1928093487,
  inCharset: 'utf-8',
  outCharset: 'utf-8',
  notice: 0,
  format: 'jsonp'
};

// jsonp的选择项
export const options = {
  param: 'jsonpCallback'
};

// 状态码
export const common = {
  SUCC_OK: 0
};
