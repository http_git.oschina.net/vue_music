// 创建song对象
export default class Song {
  constructor({id, mid, singer, name, album, duration, image, url}) {
    this.id = id;
    this.mid = mid;
    this.singer = singer;
    this.name = name;
    this.album = album;
    this.duration = duration;
    this.image = image;
    this.url = url;
  }
};

// 扩展工厂方法
export const createSong = (musicData) => {
  return new Song({
    id: musicData.songid,
    mid: musicData.songmid,
    singer: filterSinger(musicData.singer),
    name: musicData.songname,
    album: musicData.albumname,
    duration: musicData.interval,
    image: `https://y.gtimg.cn/music/photo_new/T002R300x300M000${musicData.albummid}.jpg?max_age=2592000`,
    url: `http://isure.stream.qqmusic.qq.com/C400${musicData.songmid}.m4a?fromtag=66`
  });
};

// 播放音乐的地址
// http://isure.stream.qqmusic.qq.com/C400001TXSYu1Gwuwv.m4a?vkey=B97788AE3767650498B22B6BCD73EC6E8CDA94F17DFF6E18561A86DCA110F3C4DF0C0C47ECA21698E2881C7F25C4975AAFC969C54CD1EAAD&guid=253562445&uin=0&fromtag=66

// 拼接歌手 薛之谦/周杰伦 ----<菊花台>
function filterSinger(singer) {
  console.log('....开始filterSinger的执行....');
  // let singer = [{'id': 5062, 'mid': '002J4UUk29y8BY', 'name': '薛之谦'}, {'id': 5062, 'mid': '002J4UUk29y8BZ', 'name': '薛之谦2'}];
  let ret = [];
  if (!singer) {
    return '';
  }
  singer.forEach((s) => {
    ret.push(s.name);
  });

  // console.log('数组ret进行join拼接', ret.join('/')); // 执行的结果: 数组ret进行join拼接 薛之谦/薛之谦2
  return ret.join('/');
};
