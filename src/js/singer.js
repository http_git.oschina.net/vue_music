export default class Singer {
  constructor({id, name, mid}) { // 构造函数的参数格式
    this.id = id;
    this.name = name;
    this.mid = mid;
    this.avatar = 'http://y.gtimg.cn/music/photo_new/T001R150x150M000' + mid + '.webp';
  }
};
