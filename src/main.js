import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import fastclick from 'fastclick';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueLazyLoad from 'vue-lazyload';
import './assets/style/index.scss'; // 引入全局样式
require('swiper/dist/css/swiper.css');

fastclick.attach(document.body);
Vue.config.productionTip = false;
Vue.use(VueAwesomeSwiper);
Vue.use(VueLazyLoad, {
  loading: require('./assets/img/default.png')
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
});
