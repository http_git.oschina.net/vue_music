import * as types from './mutation-types.js';

// 设置修改数据方法
const mutation = {
  [types.SET_SINGER](state, singer) {
    console.log('mutations获取singer的数据值', singer);
    state.singer = singer;
  },

  [types.SET_PLAYING_STATE](state, flag) {
    console.log('mutations获取playing_state播放状态(开始 or 暂停)', flag);
    state.playing = flag;
  },

  [types.SET_FULL_SCREEN](state, flag) {
    state.fullScreen = flag;
  },

  [types.SET_PLAYLIST](state, list) {
    console.log('mutations获取playList播放列表数据', list);
    state.playList = list;
  },

  [types.SET_SEQUENCE_LIST](state, list) {
    console.log('mutatisons获取sequenceList的顺序播放列表', list);
    state.sequenceList = list;
  },

  [types.SET_PLAY_MODE](state, mode) {
    state.mode = mode;
  },

  [types.SET_CURRENT_INDEX](state, index) {
    state.currentIndex = index;
  }
};

export default mutation;
