import Vue from 'vue';
import Vuex from 'vuex';
import * as getters from './getters';
import * as actions from './actions';
import state from './state';
import mutations from './mutations';
// 开启打印日志
import createLogger from 'vuex/dist/logger';

Vue.use(Vuex);

// 启动测试
const debug = process.env.NODE_ENV !== 'producetion';

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations,
  strict: debug, // 开启严格模式,性能有一定的损耗
  plugins: debug ? [createLogger()] : []
});
