// 设置歌手
export const SET_SINGER = 'SET_SINGER';

// 设置播放状态(开始, 暂停)
export const SET_PLAYING_STATE = 'SET_PLAYING_STATE';

// 设置播放器全屏 or 缩小
export const SET_FULL_SCREEN = 'SET_FULL_SCREEN';

// 设置播放列表
export const SET_PLAYLIST = 'SET_PLAYLIST';

// 设置顺序播放列表
export const SET_SEQUENCE_LIST = 'SET_SEQUENCE_LIST';

// 设置播放模式
export const SET_PLAY_MODE = 'SET_PLAY_MODE';

// 设置当前的索引
export const SET_CURRENT_INDEX = 'SET_CURRENT_INDEX';
