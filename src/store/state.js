import {playMode} from '@/js/config.js';

// 设置state.js文件内容，同时需要设置getters.js(获取state的数据),mutations-types.js(修改数据的类型),mutations.js(如何修改数据)
const state = {
  singer: {},
  playing: false, // 播放器进行 or 暂停
  fullScreen: false, // 播放器全屏 or 缩小
  playList: {}, // 播放列表
  sequenceList: {}, // 顺序播放列表 or 随机播放列表
  mode: playMode.sequence, // 播放模式:随机、顺序、单曲循环
  currentIndex: -1 // 当前播放的索引,进行 or 回退
};

export default state;
