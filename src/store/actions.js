import * as types from './mutation-types.js';

export const selectPlay = ({commit, state}, {item, index}) => {
  commit(types.SET_SEQUENCE_LIST, item); // 顺序播放列表
  commit(types.SET_PLAYLIST, item); // 播放列表
  commit(types.SET_CURRENT_INDEX, index); // 当前播放的索引
  commit(types.SET_FULL_SCREEN, true); // 播放的模式(标准 or 迷你)
  commit(types.SET_PLAYING_STATE, true); // 播放状态(开始 or 暂停) SET_PLAYING_STATE
};

// 设置数据, 需要提交mutations, 由于多次操作mutations, 因此需要封装一个actions封装了mutatitons中一系列操作.{commit,state}与{item,index}对象值。
