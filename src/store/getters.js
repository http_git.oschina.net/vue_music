// 获取歌手
export const singer = state => state.singer;

// 播放器进行 or 暂停 --对应 SET_PLAYING_STATE
export const playing = state => state.playing;

// 播放器全屏 or 缩小
export const fullScreen = state => state.fullScreen;

// 播放列表
export const playList = state => state.playList;

// 顺序播放列表 or 随机播放列表
export const sequenceList = state => state.sequenceList;

// 播放模式:随机、顺序、单曲循环
export const mode = state => state.mode;

// 当前播放的索引,进行 or 回退
export const currentIndex = state => state.currentIndex;

// 当前的歌曲
export const currentSong = (state) => {
  return state.playList[state.currentIndex] || {};
};
