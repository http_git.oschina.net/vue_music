import Vue from 'vue';
import Router from 'vue-router';
import {routerApp} from './router';

Vue.use(Router);

const router = new Router({
  mode: 'history', // 访问路径不带#号
  routes: routerApp, // 数组结构
  linkActiveClass: 'active',
  scrollBehavior (to, from, savedPosition) { // 这个功能只在支持 history.pushState 的浏览器中可用
    console.log('路由vue-router的scrollBehavior的方法', savedPosition);
    if (savedPosition) {
      // setTimeout(() => {
      //   window.scrollTo(savedPosition.x, savedPosition.y);
      // }, 200);
      return savedPosition;
    } else {
      if (from.meta.keepAlive) { // keep-alive来达到后退时不刷新数据，前进时刷新数据的效果
        from.meta.savedPosition = document.body.scrollTop;
      }
      return { x: 0, y: to.meta.savedPosition || 0 };
    }
  }
});

// 全局路由钩子函数
router.beforeEach((to, from, next) => {
  // 判断是否需要登陆的权限
  if (to.matched.some(res => res.meta.requireAuth)) {
    // 判断是否登录
    if (localStorage.getItem('username')) {
      next();
    } else {
    // 没有登录跳到登录页面
      next({
        path: '/register',
        query: {redirect: to.fullPath}
      });
    }
  } else {
    next();
  }
});

export default router;
