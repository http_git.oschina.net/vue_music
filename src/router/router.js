
export const appRouter = [{
  path: '/',
  name: '推荐',
  redirect: '/recommend'
}, {
  path: '/recommend',
  name: '推荐',
  component: resolve => { require(['@/components/recommend/recommend'], resolve); }
}, {
  path: '/singer',
  name: '歌手',
  component: resolve => { require(['@/components/singer/singer'], resolve); },
  children: [{
    path: ':id', // 根据id挂载详情页
    name: '歌手详情',
    component: resolve => { require(['@/components/singer-detail/singer-detail'], resolve); }
  }]
}, {
  path: '/rank',
  name: '排名',
  component: resolve => { require(['@/components/rank/rank'], resolve); }
}, {
  path: '/search',
  name: '搜索',
  component: resolve => { require(['@/components/search/search'], resolve); }
}, {
  path: '/user',
  name: '个人中心',
  meta: {
    requireAuth: true // 需要登录才能进入的页面可以增加一个meta属性
    // keepAlive: true  user会被缓存
    // <keep-alive>
    // <router-view v-if="$route.meta.keepAlive"></router-view>
    // </keep-alive>
  },
  component: resolve => { require(['@/components/user-center/user-center'], resolve); }
}];

export const routerApp = [
  ...appRouter // 此处必须使用...进行合并
];
