import jsonp from '@/util/jsonp.js';
import {commonParam, options} from '@/util/common.js';

// 方法一
export const getSingerList = () => {
  let url = 'https://u.y.qq.com/cgi-bin/musicu.fcg';
  let data = Object.assign({}, commonParam, {
    loginUin: 1627951582,
    hostUin: 0,
    needNewCode: 0,
    platform: 'yqq',
    data: '{"comm":{"ct":24,"cv":10000},"singerList":{"module":"Music.SingerListServer","method":"get_singer_list","param":{"area":-100,"sex":-100,"genre":-100,"index":-100,"sin":0,"cur_page":1}}}'
  });
  let options = {param: 'callback'};
  return jsonp(url, data, options);
};

// 歌手详情页
export const getSingerDetail = (singerId) => {
  let url = 'https://c.y.qq.com/v8/fcg-bin/fcg_v8_singer_track_cp.fcg';
  let data = Object.assign({}, commonParam, {
    loginUin: 1627951582,
    hostUin: 0,
    needNewCode: 0,
    platform: 'yqq',
    order: 'listen',
    begin: 0,
    num: 30,
    songstatus: 1,
    singermid: singerId,
    notice: 0
  });
  return jsonp(url, data, options);
};
