import jsonp from '@/util/jsonp.js';
import {commonParam, options} from '@/util/common.js';
import axios from 'axios';

export const getRecommend = () => {
  let url = 'https://c.y.qq.com/musichall/fcgi-bin/fcg_yqqhomepagerecommend.fcg';
  let data = Object.assign({}, commonParam, {
    platform: 'h5',
    uin: 0,
    needNewCode: 1
  });
  return jsonp(url, data, options);
};

/**
 * 若不添加format限制,则返回MusicJsonCallback({"code“ 的参数
 */
export const getDisList = () => {
  let url = '/api/getDiscList';
  let data = Object.assign({}, commonParam, {
    platform: 'yqq',
    hostUin: 0,
    sin: 0,
    ein: 29,
    sortId: 5,
    needNewCode: 0,
    categoryId: 10000000,
    rnd: Math.random(),
    format: 'json'
  });
  console.log('从后端转到前端axios请求');

  return axios.get(url, {
    params: data
  }).then((res) => {
    return Promise.resolve(res.data);
  });
};
