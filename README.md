# vue_music

> \"音乐播放器\"

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

#网易云后台api
https://binaryify.github.io/NeteaseCloudMusicApi/#/?id=neteasecloudmusicapi

#字符串的应用：
1.sort按照字母的升序进行排序
2.hot.concat(res);//连接两个数组
3.substr
4.阻止事件冒泡stop.prevent

#原理简介
核心文件则是在 Store ,在这里使用Vuex统一管理页面切换动画，歌曲播放状态，歌曲进度等信息。所有对于歌曲的操作都通过Vuex来进行全局管理，然后对相应的变化做出全局改变

